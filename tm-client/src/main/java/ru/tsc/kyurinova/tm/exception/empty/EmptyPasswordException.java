package ru.tsc.kyurinova.tm.exception.empty;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error. Password is empty.");
    }

    public EmptyPasswordException(String value) {
        super("Error." + value + " Password is empty.");
    }

}
