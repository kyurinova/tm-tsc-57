package ru.tsc.kyurinova.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;

@Component
public class ArgumentsShowListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String command() {
        return "arguments";
    }

    @NotNull
    @Override
    public String arg() {
        return "-arg";
    }

    @NotNull
    @Override
    public String description() {
        return "Display list of arguments...";
    }

    @Override
    @EventListener(condition = "@argumentsShowListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractListener listener : listeners) {
            @Nullable final String argument = listener.arg();
            if (argument != null && !argument.isEmpty())
                System.out.println(argument + ": " + listener.description());
        }
    }
}
