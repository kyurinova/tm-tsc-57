package ru.tsc.kyurinova.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.kyurinova.tm.event.ConsoleEvent;
import ru.tsc.kyurinova.tm.listener.AbstractListener;
import ru.tsc.kyurinova.tm.enumerated.Role;

@Component
public class DataBinSaveListener extends AbstractListener {

    @NotNull
    @Override
    public String command() {
        return "data-save-bin";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save binary data";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@dataBinSaveListener.command() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {        adminDataEndpoint.dataBinarySave(sessionService.getSession());
    }

    @Override
    public @Nullable
    Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
