package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kyurinova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.marker.UnitCategory;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class SessionRepositoryTest {

    /*@NotNull
    @Autowired
    private EntityManager entityManager;

    @NotNull
    @Autowired
    private ISessionDTORepository sessionRepository;

    @NotNull
    @Autowired
    private IUserDTORepository userRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;


    @NotNull
    private SessionDTO session;

    @NotNull
    private String sessionId;

    @NotNull
    private final String userId;

    public EntityManager GetEntityManager() {
        return entityManager;
    }

    public SessionRepositoryTest() {
        entityManager.getTransaction().begin();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin("test");
        userId = user.getId();
        @NotNull final String password = "test";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Before
    public void before() {
        entityManager.getTransaction().begin();
        session = new SessionDTO();
        sessionId = session.getId();
        session.setUserId(userId);
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        entityManager.getTransaction().commit();
    }

    @Test
    @Category(UnitCategory.class)
    public void existsSessionTest() {
        sessionRepository.add(session);
        Assert.assertNotNull(sessionRepository.findById(sessionId));
    }

    @Test
    @Category(UnitCategory.class)
    public void removeSessionByIdTest() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(sessionId);
        entityManager.getTransaction().begin();
        sessionRepository.removeById(sessionId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @After
    public void after() {
        entityManager.getTransaction().begin();
        sessionRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }*/

}
