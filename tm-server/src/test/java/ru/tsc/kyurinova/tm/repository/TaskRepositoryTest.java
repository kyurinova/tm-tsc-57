package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tsc.kyurinova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.marker.UnitCategory;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.sql.Timestamp;

public class TaskRepositoryTest {

    /*@NotNull
    @Autowired
    private EntityManager entityManager;

    @NotNull
    @Autowired
    private IUserDTORepository userRepository;

    @NotNull
    @Autowired
    private ITaskDTORepository taskRepository;

    @NotNull
    @Autowired
    private IProjectDTORepository projectRepository;

    @NotNull
    private TaskDTO task;

    @NotNull
    private String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private ProjectDTO project;

    @NotNull
    private String projectId;

    @NotNull
    private final static String PROJECT_NAME = "testProject";

    @NotNull
    private final String userId;

    public EntityManager GetEntityManager() {
        return entityManager;
    }

    public TaskRepositoryTest() {
        entityManager.getTransaction().begin();
        @NotNull final UserDTO user = new UserDTO();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 3, "test"));
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Before
    public void before() {
        entityManager.getTransaction().begin();
        task = new TaskDTO();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setCreated(new Timestamp(task.getCreated().getTime()));
        taskRepository.add(userId, task);
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(PROJECT_NAME);
        project.setCreated(new Timestamp(project.getCreated().getTime()));
        projectRepository.add(userId, project);
        entityManager.getTransaction().commit();
    }

    @Test
    @Category(UnitCategory.class)
    public void findTaskTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task.getId(), taskRepository.findById(userId, taskId).getId());
        Assert.assertEquals(task.getId(), taskRepository.findByIndex(userId, 0).getId());
        Assert.assertEquals(task.getId(), taskRepository.findByName(userId, taskName).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void existsTaskTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertEquals(task.getId(), taskRepository.findById(userId, taskId).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByIdTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.removeById(userId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAllUserId(userId).isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeTaskByNameTest() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.removeByName(userId, taskName);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.startById(userId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByIndexTest() {
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.startByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void startByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.startByName(userId, taskName);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.finishById(userId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByIndexTest() {
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.finishByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void finishByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.finishByName(userId, taskName);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIdTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusById(userId, taskId, Status.COMPLETED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusById(userId, taskId, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByIndexTest() {
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void changeStatusByNameTest() {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByName(userId, taskName, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByName(userId, taskName, Status.COMPLETED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByName(userId, taskName, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    @Category(UnitCategory.class)
    public void findByProjectAndTaskIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task.getId(), taskRepository.findByProjectAndTaskId(userId, projectId, taskId).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void findAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task.getId(), taskRepository.findAllTaskByProjectId(userId, projectId).get(0).getId());
    }

    @Test
    @Category(UnitCategory.class)
    public void bindTaskToProjectByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    @Category(UnitCategory.class)
    public void unbindTaskByIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        entityManager.getTransaction().begin();
        taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    @Category(UnitCategory.class)
    public void removeAllTaskByProjectIdTest() {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final TaskDTO task1 = new TaskDTO();
        task1.setUserId(userId);
        entityManager.getTransaction().begin();
        taskRepository.add(userId, task1);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        entityManager.getTransaction().commit();
        Assert.assertEquals(2, taskRepository.findAllTaskByProjectId(userId, projectId).size());
        entityManager.getTransaction().begin();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(0, taskRepository.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void after() {
        entityManager.getTransaction().begin();
        taskRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        projectRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }*/

}
