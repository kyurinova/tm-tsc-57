package ru.tsc.kyurinova.tm.service.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kyurinova.tm.api.service.ILogService;
import ru.tsc.kyurinova.tm.api.service.dto.IProjectTaskDTOService;
import ru.tsc.kyurinova.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kyurinova.tm.exception.system.DBException;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@AllArgsConstructor
public class ProjectTaskDTOService extends AbstractDTOService implements IProjectTaskDTOService {

    public IProjectDTORepository getProjectRepository() {
        return context.getBean(IProjectDTORepository.class);
    }

    public ITaskDTORepository getTaskRepository() {
        return context.getBean(ITaskDTORepository.class);
    }

    @NotNull
    @Autowired
    private ILogService logService;

    @Override
    public void bindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        try {
            entityManager.getTransaction().begin();
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskById(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        if (taskRepository.findById(userId, taskId) == null) throw new TaskNotFoundException();
        try {
            entityManager.getTransaction().begin();
            taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        removeAllTaskByProjectId(userId, projectId);
        @NotNull final IProjectDTORepository projectRepository = getProjectRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        if (projectRepository.findById(userId, projectId) == null) throw new ProjectNotFoundException();
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeById(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            logService.error(e);
            entityManager.getTransaction().rollback();
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllTaskByProjectId(@Nullable final String userId, @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final ITaskDTORepository taskRepository = getTaskRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (@NotNull final Exception e) {
            logService.error(e);
            throw new DBException();
        } finally {
            entityManager.close();
        }
    }

}
