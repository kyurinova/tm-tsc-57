package ru.tsc.kyurinova.tm.repository.dto;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.api.repository.dto.ISessionDTORepository;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class SessionDTORepository extends AbstractDTORepository implements ISessionDTORepository {

    @Override
    public void add(@NotNull SessionDTO session) {
        entityManager.persist(session);
    }

    @Override
    public @Nullable
    SessionDTO findById(@NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m WHERE m.id = :id";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull SessionDTO session) {
        entityManager.remove(session);
    }

    @Override
    public @Nullable
    List<SessionDTO> findAll() {
        return entityManager.createQuery("SELECT m FROM SessionDTO m", SessionDTO.class)
                .getResultList();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM SessionDTO").
                executeUpdate();
    }

    @Override
    public @Nullable
    SessionDTO findByIndex(@NotNull Integer index) {
        @NotNull final String jpql = "SELECT m FROM SessionDTO m";
        return entityManager.createQuery(jpql, SessionDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByIndex(@NotNull Integer index) {
        @Nullable final SessionDTO sessionDTO = findByIndex(index);
        entityManager.remove(sessionDTO);
    }

    @Override
    public void removeById(@NotNull String id) {
        @Nullable final SessionDTO sessionDTO = findById(id);
        entityManager.remove(sessionDTO);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM SessionDTO m";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult().intValue();
    }
}
