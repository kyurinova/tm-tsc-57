package ru.tsc.kyurinova.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:config.properties")
public class PropertyService implements IPropertyService {

    @Value("#{environment['password.secret']}")
    public String passwordSecret;

    @Value("#{environment['password.iteration']}")
    public Integer passwordIteration;

    @Value("#{environment['session.secret']}")
    public String sessionSecret;

    @Value("#{environment['session.iteration']}")
    public Integer sessionIteration;

    @Value("#{environment['application.version']}")
    public String applicationVersion;

    @Value("#{environment['developer.name']}")
    public String developerName;

    @Value("#{environment['developer.email']}")
    public String developerEmail;

    @Value("#{environment['server.host']}")
    public String serverHost;

    @Value("#{environment['server.port']}")
    public String serverPort;

    @Value("#{environment['jdbc.user']}")
    public String jdbcUser;

    @Value("#{environment['jdbc.password']}")
    public String jdbcPassword;

    @Value("#{environment['jdbc.url']}")
    public String jdbcUrl;

    @Value("#{environment['jdbc.driver']}")
    public String jdbcDriver;

    @Value("#{environment['jdbc.sql_dialect']}")
    public String jdbcSqlDialect;

    @Value("#{environment['jdbc.hbm2ddl_auto']}")
    public String jdbcNbm2ddlAuto;

    @Value("#{environment['jdbc.show_sql']}")
    public String jdbcShowSql;

    @Value("#{environment['database.format_sql']}")
    public String formatSql;

    @Value("#{environment['database.second_lvl_cache']}")
    public String secondLevelCache;

    @Value("#{environment['database.factory_class']}")
    public String cacheRegionFactory;

    @Value("#{environment['database.use_query_cache']}")
    public String useQueryCache;

    @Value("#{environment['database.use_min_puts']}")
    public String useMinPuts;

    @Value("#{environment['database.region_prefix']}")
    public String cacheRegionPrefix;

    @Value("#{environment['database.config_file_path']}")
    public String cacheProviderConfig;

}
