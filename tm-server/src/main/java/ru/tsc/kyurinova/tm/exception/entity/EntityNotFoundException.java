package ru.tsc.kyurinova.tm.exception.entity;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity not found.");
    }

}
