package ru.tsc.kyurinova.tm.repository.model;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.tsc.kyurinova.tm.api.repository.model.ISessionRepository;
import ru.tsc.kyurinova.tm.model.Session;

import java.util.List;

@Repository
@Scope("prototype")
@AllArgsConstructor
public class SessionRepository extends AbstractRepository implements ISessionRepository {

    @Override
    public void add(@NotNull Session session) {
        entityManager.persist(session);
    }

    @Override
    public @Nullable
    Session findById(@NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE m.id = :id";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void remove(@NotNull Session session) {
        entityManager.remove(session);
    }

    @Override
    public @Nullable
    List<Session> findAll() {
        return entityManager.createQuery("SELECT m FROM Session m", Session.class)
                .getResultList();
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM Session").
                executeUpdate();
    }

    @Override
    public @Nullable
    Session findByIndex(@NotNull Integer index) {
        @NotNull final String jpql = "SELECT m FROM Session m";
        return entityManager.createQuery(jpql, Session.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByIndex(@NotNull Integer index) {
        @Nullable final Session Session = findByIndex(index);
        entityManager.remove(Session);
    }

    @Override
    public void removeById(@NotNull String id) {
        @Nullable final Session Session = findById(id);
        entityManager.remove(Session);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session m";
        return entityManager.createQuery(jpql, Long.class)
                .getSingleResult().intValue();
    }
}
